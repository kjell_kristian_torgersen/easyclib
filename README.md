# README #

This project should easily import into Eclipse CDT. After it is imported another project needs to be made in order to use it. This because it is a library. The project that needs to use this library must be set up to use the include files of this project and link with the library that is generated from this project.

### The content is functions and data structures to handle ###

* Serial ports 
* Linked lists (sllist)
* Data from multiple sources (user, controlling other processes, network sockets... everything with a file descriptor) (multiplexer)
* Variable sized array (arraylist)
* Timers (not yet implemented)

Also there is Doxygen documentation. (Run command "doxygen Doxyfile" in project root, and a folder called Docs will appear with documentation) 

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact