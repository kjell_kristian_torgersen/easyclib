/*
 * timer.h
 *
 *  Created on: Jan 24, 2016
 *      Author: kjell
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <datasource.h>
#include <sys/timerfd.h>

typedef struct Timer
{
	DataSource_t * Source;
	struct itimerspec * TimerSettings;
} Timer_t;

Timer_t * TIMER_New(CallBack_t callBack);
void TIMER_SetInterval(Timer_t *timer, __time_t seconds, __syscall_slong_t nanoseconds);
void TIMER_SetOnce(Timer_t *timer, __time_t seconds, __syscall_slong_t nanoseconds);
void TIMER_Remove(Timer_t *timer);

#endif /* TIMER_H_ */
