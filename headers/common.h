/*
 * common.h
 *
 *  Created on: Jan 18, 2016
 *      Author: kjell
 */

#ifndef COMMON_H_
#define COMMON_H_

void COM_memoryErrorCheck(void *ptr);
void COM_Free(void *ptr);
void *COM_MallocGC(size_t n);
void COM_FreeGC(void *ptr);
void COM_Init(void);
void* COM_memdup(const void* data, size_t n);

#endif /* COMMON_H_ */
