#ifndef MULTIPLEXER_H_
#define MULTIPLEXER_H_

#include "block.h"
#include "sllist.h"
#include "datasource.h"

typedef struct Multiplexer /** Multiplexer object containing a read buffer and a list of DataSources to monitor */
{
	Block_t * ReadBuffer; /**< Just a buffer used for temporarily store received data to give into callback functions*/
	SLList_t * DataSources; /**< Linked list of all datasources that should be monitored*/
} Multiplexer_t;

Multiplexer_t * MP_New(size_t readBufferSize);
void MP_AddDatasource(Multiplexer_t *mp, DataSource_t *ds);
void MP_Delete(Multiplexer_t *mp);
void MP_Remove(Multiplexer_t *mp);
void MP_Select(Multiplexer_t *mp);

#endif /* MULTIPLEXER_H_ */
