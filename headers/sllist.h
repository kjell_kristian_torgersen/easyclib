/*
 * sllist.h
 *
 *  Created on: Jan 18, 2016
 *      Author: kjell
 */

#ifndef SLLIST_H_
#define SLLIST_H_

#include "block.h"

typedef int (*Comparator)(void*, void*);

typedef struct SLLNode /** Data structure for handling a single node in the singly linked list*/
{
	size_t Size; /**< Size of the element in bytes. 0 means it is a reference which shouldn't be deleted when node is*/
	char * Data; /**< User specified element, any kind of data as block of memory*/
	struct SLLNode * Next; /**< Next node in the singly linked list*/
} SLLNode_t;

typedef struct SLList /** Data structure for handling a singly linked list*/
{
	struct SLLNode *First; /**< First node of list*/
	struct SLLNode *Last; /**< Last node of list, to access this easily*/
	int Nodes;	/**< Number of nodes in list*/
} SLList_t;

SLLNode_t * SLLNode_New(void *data, size_t n);
SLLNode_t * SLLNode_Ref(void *data);
SLLNode_t * SLLNode_Str(char *str);

SLList_t * SLList_New(void);

void SLList_Add(SLList_t *list, SLLNode_t *node);
void SLList_AddFirst(SLList_t *list, SLLNode_t * node);

void SLList_ForEach(SLList_t * list, void (*function)(void*,size_t));
int SLList_DeleteNodeP(SLList_t * list, void * Element, Comparator comparator);
/*void SLList_InsertBefore(SLList_t *list, void *oldElement, void *newElement);*/
/*void SLList_InsertAfter(SLList_t *list, void *oldElement, void *newElement);*/
void * SLList_Remove(SLList_t *list, void *element);
void SLList_Delete(SLList_t *list, void *element);
void SLList_DeleteList(SLList_t *list);
void SLList_RemoveList(SLList_t *list);
void SLList_MergeSort(SLList_t * list, Comparator compare);

#endif /* SLLIST_H_ */
