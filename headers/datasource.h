#ifndef SOURCE_H_
#define SOURCE_H_

#include "block.h"

/** Callback function for when data is received from a datasource */
typedef void (*CallBack_t)(void *extra, int fd, Block_t *data);


typedef struct DataSource /** Datasource object for holding necessary informtion to monitor a file descriptor*/
{
	int fd; /**< File descriptor*/
	CallBack_t CallBack; /**< Callback function for when data appear on file descriptor fd*/
	void *Extra; /**< Pointer for additional data structure which may be useful for the callback function*/
} DataSource_t;

DataSource_t * DataSource_Init(void *extra, int fd, CallBack_t CallBack);
void DataSource_Remove(DataSource_t *dataSource);

#endif
