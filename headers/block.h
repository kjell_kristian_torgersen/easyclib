/*
 * block.h
 *
 *  Created on: Jan 18, 2016
 *      Author: kjell
 */

#ifndef BLOCK_H_
#define BLOCK_H_

#include <stddef.h>

typedef struct Block /** Block data structure for passing memory chunks between functions */
{
	size_t Size;	/**< Size of data block in bytes*/
	unsigned char * Data;	/**< The data in itself*/
} Block_t;

void BLOCK_Delete(Block_t * block);
void BLOCK_Remove(Block_t * block);
Block_t * BLOCK_FromString(const char *str);
Block_t * BLOCK_CopyFrom(const void *data, size_t size);
Block_t * BLOCK_Duplicate(const Block_t *block);
Block_t * BLOCK_New(void *data, size_t size);
Block_t * BLOCK_Create(size_t size);

#endif /* BLOCK_H_ */
