/*
 * terminal.h
 *
 *  Created on: Jan 24, 2016
 *      Author: kjell
 */

#ifndef TERMINAL_H_
#define TERMINAL_H_

typedef enum Color /** Colors for ANSI terminal codes */
{
	COL_BLACK,
	COL_RED,
	COL_GREEN,
	COL_BROWN,
	COL_BLUE,
	COL_MAGENTA,
	COL_CYAN,
	COL_GREY
} Color_t;

void TERM_Color(Color_t col);
void TERM_Goto(int line, int column);
void TERM_Clear(void);
void TERM_DisableUserEcho(void);
void TERM_EnableUserEcho(void);

#endif /* TERMINAL_H_ */
