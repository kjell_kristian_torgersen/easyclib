/*
 * packagehandler.h
 *
 *  Created on: Jan 24, 2016
 *      Author: kjell
 */

#ifndef PACKAGEHANDLER_H_
#define PACKAGEHANDLER_H_

/** This can be set to whatever byte you want to trigger on*/
#define START_BYTE 0xAA

/** This callback is for functions that should be called when a valid package is received. The content of the Block Package is just the package payload and size. No checksum or anything else.*/
typedef void (*PackageReceivedCallback)(Block_t * Package);

typedef struct PackageHandler /** Necessary information for receiving a package and callback-function for when packages are successfully received.*/
{
	unsigned int PackageIndex;
	unsigned int PackageChecksum;
	Block_t *Package;
	PackageReceivedCallback InterpretPackage;
} PackageHandler_t;

PackageHandler_t * PH_New(PackageReceivedCallback callBack);
void PH_ReceivePackage(PackageHandler_t * ph, Block_t *data);
void PH_Remove(PackageHandler_t * ph);

#endif /* PACKAGEHANDLER_H_ */
