/*
 * arraylist.h
 *
 *  Created on: Jan 23, 2016
 *      Author: kjell
 */

#ifndef ARRAYLIST_H_
#define ARRAYLIST_H_

#include <stddef.h>

/** Variable sized array that automatically grows */
typedef struct ArrayList
{
	char * Data;  /**< Pointer to data in array*/
	int Capacity; /**< Current capacity of array in number of elements*/
	int Count;    /**< Number of elements in array for the moment*/
	size_t ElemSize; /**< Size of one element in bytes*/
} ArrayList_t;

void AL_ChangeCapacity(ArrayList_t *al, size_t newCapacity);
ArrayList_t * AL_New(size_t ElemSize);
void AL_Add(ArrayList_t *al, void *element);
void AL_Delete(ArrayList_t *al);

#endif /* ARRAYLIST_H_ */
