/*
 * serialport.h
 *
 *  Created on: Jan 18, 2016
 *      Author: kjell
 */

#ifndef SERIALPORT_H_
#define SERIALPORT_H_

typedef struct SerialPort /** Object for keeping track of a serialport*/
{
	DataSource_t *Source; /**< The serial port as a DataSource object*/
	struct termios oldtio; /**< The old settings for the serial port (to set it back to before closing it)*/
} SerialPort_t;

SerialPort_t * SerialPort_New(char *device, tcflag_t baud, CallBack_t callBack);
void SerialPort_Delete(SerialPort_t *sp);
void SerialPort_Close(SerialPort_t *sp);



#endif /* SERIALPORT_H_ */
