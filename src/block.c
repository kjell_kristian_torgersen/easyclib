/** \file block.c Encapsulate a block of data and amount of bytes in a structure. */
/*
 * block.c
 *
 *  Created on: Jan 18, 2016
 *      Author: kjell
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "block.h"
#include "common.h"

char *strdup(const char *s);

/** \brief Delete Block and free the associated memory block. \warning Might cause free() will be called on the same data twice if used incorrectly.
 * \param *block Block to delete*/
void BLOCK_Delete(Block_t * block)
{
	if(block != NULL) {
		if(block->Data != NULL) COM_FreeGC(block->Data);
		COM_FreeGC(block);
	}
}

/** \brief Delete \arg block and don't free the associated memory block. \warning Might cause memory leaks if used incorrectly.
 * \param *block Block to remove, but don't delete associated data*/
void BLOCK_Remove(Block_t * block)
{
	if(block != NULL) COM_FreeGC(block);
}

/** \brief Copy \arg size bytes from \arg data and store the whole thing in a new block
 * \param *str String to create block from
 * \return Newly created data block with unique copy of \arg data */
Block_t * BLOCK_FromString(const char *str)
{

	return BLOCK_New(strdup(str), strlen(str));
}

/** \brief Copy \arg size bytes from \arg data and store the whole thing in a new block
 * \param *data Data to make copy of
 * \param size Number of bytes to copy from *data
 * \return Newly created data block with unique copy of \arg data */
Block_t * BLOCK_CopyFrom(const void *data, size_t size)
{
	void * dataCopy = (void*)COM_MallocGC(size);
	COM_memoryErrorCheck(dataCopy);
	memcpy(dataCopy, data, size);
	return BLOCK_New(dataCopy, size);
}

/** \brief Copy a block
 * \param *source Source Block
 * \return Newly created data block with unique copy of \arg data */
Block_t * BLOCK_Duplicate(const Block_t * source)
{
	void * dataCopy = (void*)COM_MallocGC(source->Size);
	COM_memoryErrorCheck(dataCopy);
	memcpy(dataCopy, source->Data, source->Size);
	return BLOCK_New(dataCopy, source->Size);
}

/** \brief Just create a new block and set it to reference the \arg data pointer.
 * \param *data Pointer to data to reference
 * \param size Number of bytes from *data
 * \return Newly created data block with \arg data */
Block_t * BLOCK_New(void *data, size_t size)
{
	Block_t * block;
	block = (Block_t*)COM_MallocGC(sizeof(Block_t));
	COM_memoryErrorCheck(block);
	block->Data = data;
	block->Size = size;
	return block;
}

/** \brief Create a new block and allocate data.
 * \param size Number of bytes to allocate for *data
 * \return Newly created data block */
Block_t * BLOCK_Create(size_t size)
{
	void *data;
	data = COM_MallocGC(size);
	COM_memoryErrorCheck(data);
	return BLOCK_New(data, size);
}
