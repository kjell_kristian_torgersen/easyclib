/** \file datasource.c Keep track of datasources, which can be network streams, serial ports, controlling other programs, timers and communication with the user (anything with a file descriptor). This data structure is supposed to be used together with the Multiplexer. */

#include <stdio.h>
#include <stdlib.h>
#include "common.h"
#include "datasource.h"
#include "block.h"

/** \brief Create a container for a datasource. The Datasource is given with the file descriptor fd
 * \param fd File descriptor to monitor
 * \param CallBack Callback-function to call when data is received on file descriptor fd
 * \return DataSource object.*/
DataSource_t * DataSource_Init(void *extra, int fd, CallBack_t CallBack)
{
	DataSource_t *dataSource;
	dataSource = (DataSource_t*)COM_MallocGC(sizeof(DataSource_t));
	COM_memoryErrorCheck(dataSource);
	dataSource->fd = fd;
	dataSource->CallBack = CallBack;
	dataSource->Extra = extra;
	return dataSource;
}

/** \brief Delete a Datasource object.
 * \param  *dataSource Object to delete.*/
void DataSource_Remove(DataSource_t *dataSource)
{
	COM_Free(dataSource);
}
