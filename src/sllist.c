/** \file sllist.c Functions for creating and handling a singly linked list. This list can grow until computer runs out of memory. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sllist.h"
#include "common.h"

static SLList_t * CreateList(void);
static SLLNode_t * CreateNode(void *Element, size_t size);

/** \brief Creates a new single linked list object.
 * \return A new single linked list object which is empty */
static SLList_t * CreateList(void) {
	SLList_t * list;
	list = (SLList_t*) COM_MallocGC(sizeof(SLList_t));
	COM_memoryErrorCheck(list);
	list->First = NULL;
	list->Last = NULL;
	list->Nodes = 0;
	return list;
}

/** \brief Creates a new node for single linked list
 * \param *Element Element to store in node
 * \return A new node */
static SLLNode_t * CreateNode(void *Element, size_t size) {
	SLLNode_t * node = (SLLNode_t*) COM_MallocGC(sizeof(SLLNode_t));
	COM_memoryErrorCheck(node);
	node->Data = Element;
	node->Next = NULL;
	node->Size = size;
	return node;
}

/** \brief Create a new empty single linked list
 * \return The newly created linked list
 * */
SLList_t * SLList_New(void) {
	SLList_t * list;
	list = CreateList();
	return list;
}

SLLNode_t * SLLNode_New(void *data, size_t n) {
	return CreateNode(COM_memdup(data, n), n);
}

SLLNode_t * SLLNode_Ref(void *data) {
	return CreateNode(data, 0);
}
SLLNode_t * SLLNode_Str(char *str) {
	size_t n = strlen(str) + 1; /* +1 for including zero-terminator*/
	return CreateNode(COM_memdup(str, n), n);
}

/** \brief Copy n bytes from *data to a new node, at end of list
 * \param *list List to add to
 * \param *data Data to add/refer
 * \param n Bytes to copy. If n == 0 then a reference is made to data instead. */
void SLList_Add(SLList_t *list, SLLNode_t * node) {

	if (list == NULL) {
		fprintf(stderr, "Error: Tried to run SLList_Add with list==NULL\n");
		return;
	}

	/* Check if empty list, then First and last node is the only node*/
	if (list->Nodes == 0) {
		list->First = node;
		list->Last = node;
	} else { /* Add new node to end of list*/
		list->Last->Next = node;
		list->Last = node;
	}

	list->Nodes++;
}

/** \brief Add an Element to the beginning of a single linked list
 * \param *list List to add to
 * \param *Element Element to add
 * \param n Bytes to copy. If n == 0 then a reference is made to data. */
void SLList_AddFirst(SLList_t *list, SLLNode_t *node) {

	if (list == NULL) {
		fprintf(stderr,
				"Error: Tried to run SLList_AddFirst with list==NULL\n");
		return;
	}

	/* Check if empty list, then First and last node is the only node*/
	if (list->Nodes == 0) {
		list->First = node;
		list->Last = node;
	} else { /* Add new node to beginning of list*/
		node->Next = list->First;
		list->First = node;
	}
	list->Nodes++;
}

/** \brief Iteratively call the function \arg function on all Elements in list
 * \param *list List to iterate through
 * \param *function Function pointer to function*/
void SLList_ForEach(SLList_t * list, void (*function)(void*, size_t)) {
	SLLNode_t *iterator;
	if (list == NULL) {
		fprintf(stderr, "Error: Tried to run SLList_ForEach with list==NULL\n");
		return;
	}

	for (iterator = list->First; iterator != NULL; iterator = iterator->Next) {
		function(iterator->Data, iterator->Size);
	}
}

/** \brief Delete the node and the data if it is not a reference.
 * \param *node Node to delete */
void DeleteNode(SLLNode_t * node) {
	if (node->Size != 0)
		COM_Free(node->Data);
	COM_Free(node);
}

/** \brief Remove a node but keep data
 * \param *list List to delete from
 * \param *Element Element to search for for finding node to remove
 * \return Data in node, NULL if not found */

void * SLList_RemoveNode(SLList_t * list, void * Element) {
	SLLNode_t * iterator = list->First;
	SLLNode_t * prev = NULL;
	void *ret = NULL;
	/* Zero Nodes in list, not possible to remove Nodes*/
	if (list->Nodes == 0)
		return 0;

	while (iterator != NULL) {
		if (iterator->Data == Element) { /* Compare until we find the node we want to delete*/

			/* One node in list, just delete it and set pointers to NULL*/
			if (list->Nodes == 1) {
				ret = iterator->Data;
				COM_Free(iterator);
				list->First = NULL;
				list->Last = NULL;
				list->Nodes = 0;
				return ret;
			} else if (iterator == list->First) {
				list->First = iterator->Next; /* Make list head point to Next node*/
				ret = iterator->Data;
				COM_Free(iterator); /* Delete */
				list->Nodes--;
				return ret;
			} else if (iterator == list->Last) {
				list->Last = prev; /* Make end point to currently second last node*/
				prev->Next = NULL;
				ret = iterator->Data;
				COM_Free(iterator);
				list->Nodes--;
				return ret;
			} else {
				prev->Next = iterator->Next; /* Make the node before this point to the Next*/
				ret = iterator->Data;
				COM_Free(iterator); /* Delete this*/
				list->Nodes--;
				return ret;
			}
		}
		/* Update prev and iterator for Next Element*/
		prev = iterator;
		iterator = iterator->Next;
	}
	return ret;
}

/** \brief Remove a node and free data associated with it
 * \param *list List to delete from
 * \param *Element Element to find in node to delete
 * \return 1 if a node is deleted, 0 else*/
int SLList_DeleteNode(SLList_t * list, void * Element) {
	SLLNode_t * iterator = list->First;
	SLLNode_t * prev = NULL;

	/* Zero Nodes in list, not possible to remove Nodes*/
	if (list->Nodes == 0)
		return 0;

	while (iterator != NULL) {
		/* Compare until we find the node we want to delete*/
		if (iterator->Data == Element) {

			/* One node in list, just delete it and set pointers to NULL*/
			if (list->Nodes == 1) {
				DeleteNode(iterator);
				list->First = NULL;
				list->Last = NULL;
				list->Nodes = 0;
				return 1;
			} else if (iterator == list->First) {
				list->First = iterator->Next; /* Make list head point to Next node*/
				DeleteNode(iterator); /* Delete */
				list->Nodes--;
				return 1;
			} else if (iterator == list->Last) {
				list->Last = prev; /* Make end point to currently second last node*/
				prev->Next = NULL;
				DeleteNode(iterator);
				list->Nodes--;
				return 1;
			} else {
				prev->Next = iterator->Next; /* Make the node before this point to the Next*/
				DeleteNode(iterator); /* Delete this */
				list->Nodes--;
				return 1;
			}
		}
		/* Update prev and iterator for Next Element*/
		prev = iterator;
		iterator = iterator->Next;
	}
	return 0;
}

/** \brief Remove a node and free data associated with it
 * \param *list List to delete from
 * \param *Element Element to search for
 * \param *comparator Function for determining if the correct node is found. The comparator should return 0 on match.
 * \return 1 if a node is deleted, 0 else*/
int SLList_DeleteNodeP(SLList_t * list, void * Element, Comparator comparator) {
	SLLNode_t * iterator = list->First;
	SLLNode_t * prev = NULL;

	/* Zero Nodes in list, not possible to remove Nodes*/
	if (list->Nodes == 0)
		return 0;

	while (iterator != NULL) {
		/* Compare until we find the node we want to delete */
		if (comparator(iterator->Data, Element) == 0) {

			/* One node in list, just delete it and set pointers to NULL*/
			if (list->Nodes == 1) {
				DeleteNode(iterator);
				list->First = NULL;
				list->Last = NULL;
				list->Nodes = 0;
				return 1;
			} else if (iterator == list->First) {
				list->First = iterator->Next; /* Make list head point to Next node*/
				DeleteNode(iterator); /* Delete */
				list->Nodes--;
				return 1;
			} else if (iterator == list->Last) {
				list->Last = prev; /* Make end point to currently second last node*/
				prev->Next = NULL;
				DeleteNode(iterator);
				list->Nodes--;
				return 1;
			} else {
				prev->Next = iterator->Next; /* Make the node before this point to the Next*/
				DeleteNode(iterator); /* Delete this */
				list->Nodes--;
				return 1;
			}
		}
		/* Update prev and iterator for Next Element*/
		prev = iterator;
		iterator = iterator->Next;
	}
	return 0;
}

/** \brief Search for a node containing *Element, use the comparator function to determine which node to find.
 * \param *list List to search through
 * \param *Element Element to search for
 * \param *comparator Function returning 0 if node is found
 * \return Node found, else NULL*/
SLLNode_t * SLList_FindNodeP(SLList_t * list, void * Element,
		Comparator comparator) {
	SLLNode_t * iterator = list->First;
	while (iterator != NULL) {
		if (comparator(iterator->Data, Element) == 0) {
			return iterator;
		}
		iterator = iterator->Next;
	}
	return NULL;
}

/** \brief Search for a node containing *Element
 * \param *list List to search through
 * \param *Element Element to search for
 * \return Node found, else NULL*/
SLLNode_t * SLList_FindNode(SLList_t * list, void * Element) {
	SLLNode_t * iterator = list->First;
	while (iterator != NULL) {
		if (iterator->Data == Element) {
			return iterator;
		}
		iterator = iterator->Next;
	}
	return NULL;
}

/** \brief Delete list \arg *list, its Nodes and also free up all associated memory.  \warning If used incorrectly, already free'd memory might be free'd again.
 * \param *list List to delete*/
void SLList_DeleteList(SLList_t *list) {
	SLLNode_t *iterator;
	SLLNode_t *node;

	if (list == NULL) {
		fprintf(stderr,
				"Error: Tried to run SLList_DeleteList with list==NULL\n");
		return;
	}

	iterator = list->First;
	while (iterator != NULL) {
		COM_Free(iterator->Data);
		node = iterator;
		iterator = iterator->Next;
		COM_Free(node);
	}
	COM_Free(list);
}

/** \brief Delete list \arg *list, but keep the data in the list. This doesn't call free on each Element in the list. \warning If used incorrectly, massive memory leaks may occur.
 * \param *list List to delete */
void SLList_RemoveList(SLList_t *list) {
	SLLNode_t *iterator;
	SLLNode_t *node;
	if (list == NULL) {
		fprintf(stderr,
				"Error: Tried to run SLList_RemoveList with list==NULL\n");
		return;
	}

	iterator = list->First;
	while (iterator != NULL) {
		node = iterator;
		iterator = iterator->Next;
		COM_FreeGC(node);
	}
	COM_FreeGC(list);
}

SLLNode_t * Merge(SLLNode_t * a, SLLNode_t *b, Comparator compare) {
	SLLNode_t * result = NULL;
	if (!a)
		return b;
	if (!b)
		return a;

	if (compare(a->Data, b->Data) < 0) {
		result = a;
		result->Next = Merge(a->Next, b, compare);
	} else {
		result = b;
		result->Next = Merge(a, b->Next, compare);
	}
	return result;
}

SLLNode_t * Split(SLLNode_t *head) {
	SLLNode_t * fast = head;
	SLLNode_t * slow = head;
	SLLNode_t * temp;

	while (fast->Next && fast->Next->Next) {
		fast = fast->Next->Next;
		slow = slow->Next;
	}
	temp = slow->Next;
	slow->Next = NULL;
	return temp;
}

SLLNode_t * MergeSort(SLLNode_t *head, Comparator compare) {
	SLLNode_t *second;
	if (!head || !head->Next)
		return head;
	second = Split(head);
	head = MergeSort(head, compare);
	second = MergeSort(second, compare);
	return Merge(head, second, compare);
}

void SLList_MergeSort(SLList_t * list, Comparator compare) {
	list->First = MergeSort(list->First, compare);
	list->Last = list->First;
	while (list->Last->Next) {
		list->Last = list->Last->Next;
	}
}
