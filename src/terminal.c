/** \file terminal.c Functions for handling the terminal with ANSI escape terminal control codes. */
#include <stdio.h>
#include <unistd.h>
#include <termios.h>

#include "terminal.h"

/** \brief Change the text color.
 * \param col Choosen color in Color enumeration */
void TERM_Color(Color_t col)
{
	char str[] = "\x1b[0;30m";
	str[5] = (char)((unsigned int)str[5]+col);
	write(1,str,7);
}

/** \brief Goto line and column on screen
 * \param line Start at 1 and counts upwards
 * \param column Stat at 1 and counts upwards
 * */
void TERM_Goto(int line, int column)
{
	extern int snprintf (char *__restrict __s, size_t __maxlen,
			     const char *__restrict __format, ...)
	     __THROWNL __attribute__ ((__format__ (__printf__, 3, 4)));
	char buf[16];
	size_t n;

	n = (size_t)snprintf(buf, (size_t)16, "\033[%i;%iH",line,column);
	write(1, buf, n);
}
/** \brief Just clear the screen. */
void TERM_Clear(void)
{
	char cmd[] = "\033[2J";
	write(1, cmd, 4);
}

void TERM_DisableUserEcho(void)
{
	struct termios tattr;
	tcgetattr(STDIN_FILENO, &tattr);
	tattr.c_lflag &= (tcflag_t)~(ECHO|ICANON); /* Clear ECHO. */
	tattr.c_cc[VMIN] = 1;
	tattr.c_cc[VTIME] = 0;
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &tattr);
}

void TERM_EnableUserEcho(void)
{
	struct termios tattr;
	tcgetattr(STDIN_FILENO, &tattr);
	tattr.c_lflag |= (tcflag_t)(ECHO|ICANON); /* Clear ECHO. */
	tattr.c_cc[VMIN] = 1;
	tattr.c_cc[VTIME] = 0;
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &tattr);
}
