/** \file multiplexer.c This file contains functions for creating and using a "Multiplexer".
 * The purpose of the multiplexer is to listen for data from various data sources.
 * A DataSource object is used for encapsulating various data streams.
 * Various streams can be anything with a file descriptor, which is: serial ports, network connections, user communication and controlling other programs. (waiting for their output)
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include "common.h"
#include "datasource.h"
#include "sllist.h"
#include "multiplexer.h"

/** \brief Create a new Multiplexer object.
 * \param readBufferSize How many bytes you will receive maximum to the callback-functions you specify.
 * \return Multiplexer object*/
Multiplexer_t * MP_New(size_t readBufferSize)
{
	Multiplexer_t *mp = (Multiplexer_t*) COM_MallocGC(sizeof(Multiplexer_t));
	COM_memoryErrorCheck(mp);
	mp->ReadBuffer = BLOCK_New(COM_MallocGC(readBufferSize), readBufferSize);
	mp->DataSources = SLList_New();
	return mp;
}

/** \brief Add a DataSource *ds to an Multiplexer object.
 * \param *mp Multiplexer to add to.
 * \param *ds DataSource object to add. */
void MP_AddDatasource(Multiplexer_t *mp, DataSource_t *ds)
{
	SLList_Add(mp->DataSources, SLLNode_Ref(ds));
}

/** \brief Delete a Multiplexer object.
 * \param *mp Multiplexer to delete */
void MP_Delete(Multiplexer_t *mp)
{
	if (mp == NULL)
		return;
	SLList_DeleteList(mp->DataSources);
	BLOCK_Delete(mp->ReadBuffer);
	COM_FreeGC(mp);
}

/** \brief Delete a Multiplexer object but not its DataSources.
 * \param *mp Multiplexer to delete */
void MP_Remove(Multiplexer_t *mp)
{
	if (mp == NULL)
		return;
	SLList_RemoveList(mp->DataSources);
	BLOCK_Delete(mp->ReadBuffer);
	COM_FreeGC(mp);
}

/** \brief Setup and run the select() system call on Multiplexer *mp. This function will wait until data is received on one of the data sources and call the appropriate Callback functions.
 * \param *mp Multiplexer to wait for data on. */
void MP_Select(Multiplexer_t *mp)
{
	DataSource_t *dataSource;
	SLLNode_t *iterator = NULL;
	ssize_t n;
	int maxfd = -1;
	Block_t * receivedData;
	fd_set readfds;

	FD_ZERO(&readfds);

	/* Loop through the list and setup all file descriptors for monitoring*/
	for (iterator = mp->DataSources->First; iterator != NULL; iterator = iterator->Next) {
		dataSource = (DataSource_t*) iterator->Data; /* Extract DataSource object from list node*/
		if (dataSource->fd > maxfd) /* Keep track of highest file descriptor value*/
			maxfd = dataSource->fd;
		FD_SET(dataSource->fd, &readfds); /* Set file descriptor for monitoring*/
	}

	select(maxfd + 1, &readfds, NULL, NULL, NULL);


	/* Loop through the list and run the callback function for all file descriptors that have new data*/
	for (iterator = mp->DataSources->First; iterator != NULL; iterator = iterator->Next) {
		dataSource = (DataSource_t*) iterator->Data; /* Extract DataSource object from list node*/
		if (FD_ISSET(dataSource->fd, &readfds)) { /* Check if data available */
			/* Yes, read n bytes and store into ReadBuffer object*/
			n = read(dataSource->fd, mp->ReadBuffer->Data,
					mp->ReadBuffer->Size);
			/* Create a new fixed size datablock of size n with received data*/
			receivedData = BLOCK_CopyFrom(mp->ReadBuffer->Data, (size_t)n);

			dataSource->CallBack(dataSource->Extra, dataSource->fd, receivedData); /* Execute user specified callback function*/
			BLOCK_Delete(receivedData);
		}
	}

}
