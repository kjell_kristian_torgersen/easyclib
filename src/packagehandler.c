/*
 * packagehandler.c
 *
 *  Created on: Jan 24, 2016
 *      Author: kjell
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "block.h"
#include "packagehandler.h"
#include "common.h"

/** \brief Create a new handler for receiving data packages
 * \param callBack function to call when a datapackage is successfully received
 * \return PackageHandler object */
PackageHandler_t * PH_New(PackageReceivedCallback callBack)
{
	PackageHandler_t *ph;
	ph = (PackageHandler_t*) COM_MallocGC(sizeof(PackageHandler_t));
	ph->InterpretPackage = callBack;
	ph->Package = NULL;
	ph->PackageChecksum = 0;
	ph->PackageIndex = 0;
	return ph;
}

/** \brief Parse a Block of data that may contain valid package data
 * \param ph PackageHandler object to use
 * \param data Block with data received from somewhere (typically SerialPort ) */
void PH_ReceivePackage(PackageHandler_t * ph, Block_t *data)
{
	unsigned char b;
	unsigned int i;
	for (i = 0; i < data->Size; i++) {
		b = data->Data[i];
		switch (ph->PackageIndex) {
		case 0:
			if (b == START_BYTE) {
				ph->PackageIndex = 1;
				ph->PackageChecksum = b;
			}
			break;
		case 1:
			ph->Package = BLOCK_Create(b);
			ph->PackageChecksum += b;
			ph->PackageIndex = 2;
			break;
		default:
			if (ph->PackageIndex - 2 < ph->Package->Size) {
				ph->Package->Data[ph->PackageIndex - 2] = b;
			}
			ph->PackageChecksum += b;
			ph->PackageIndex++;
			if (ph->PackageIndex >= ph->Package->Size + 3) {
				ph->PackageIndex = 0;
				while (ph->PackageChecksum >= 0x100) {
					ph->PackageChecksum = (ph->PackageChecksum >> 8) + (ph->PackageChecksum & 0xFF);
				}
				ph->PackageChecksum = (~ph->PackageChecksum) & 0xFF;
				if (ph->PackageChecksum == 0) {
					ph->InterpretPackage(ph->Package);
				} else {
				}
				BLOCK_Delete(ph->Package);
				ph->Package = NULL;
			}
			break;
		}
	}
}

/** \brief Create a package from the Block *payload with checksum etc..*/
Block_t * PH_CreatePackage(Block_t *payload)
{
	unsigned int i;
	unsigned int checksum = 0;
	Block_t * package;
	if(payload->Size > 255) {
		fprintf(stderr, "Cannot create packages greater than 255 bytes!");
		exit(-1);
	}
	package = BLOCK_Create(payload->Size+3);
	package->Data[0] = START_BYTE;
	package->Data[1] = (unsigned char)(payload->Size);
	memcpy(&package->Data[2], payload->Data, payload->Size);
	for(i = 0; i < package->Size-1; i++) {
		checksum += package->Data[i];
		while(checksum > 255) checksum -= 255;
	}
	package->Data[package->Size-1] = (unsigned char)(~checksum);
	return package;
}

/** \brief Just delete a PackageHandler object*/
void PH_Remove(PackageHandler_t * ph)
{
	COM_Free(ph->Package);
	COM_Free(ph);
}
