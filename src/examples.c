/*
 ============================================================================
 Name        : testceasylib.c
 Author      : Kjell Kristian
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <termios.h>

#include "block.h"
#include "multiplexer.h"
#include "datasource.h"
#include "sllist.h"
#include "arraylist.h"
#include "serialport.h"
#include "common.h"
#include "terminal.h"
#include "timer.h"
#include "packagehandler.h"

char *logFilename = "microcontroller-log.txt";
FILE *gnuplot;
int show = 0;
int plot = 1;

void PrintList(SLList_t* list)
{
	SLLNode_t* it = NULL;
	printf("nodes=%i, ", list->Nodes);
	for (it = list->First; it != NULL; it = it->Next) {
		printf("%s ", (char*) it->Data);
	}
	printf("\n");
}

void testList2(void)
{
	int i = 0;
	SLLNode_t * iterator;
	SLList_t * list;
	char word[128];
	FILE *fin = fopen("/home/kjell/workspace/ceasylib/Examples/storfil.txt", "r");

	list = SLList_New();

	//printf("Enter whatever words you like and terminate with --- as separate word:\n");
	for (;;) {
		fscanf(fin, "%s", word);
		//printf("%s OK. \n", word);
		if (strcmp(word, "---")) {
			SLList_Add(list, SLLNode_Str(word));
		} else {
			break;
		}
		i++;
	}
	if (list->Nodes > 1) {
		/*for (iterator = list->First; iterator != NULL; iterator = iterator->Next) {
		 printf("%s ", iterator->Data);
		 }
		 printf("\n");*/
		SLList_MergeSort(list, (Comparator) strcmp);
		for (iterator = list->First; iterator != NULL; iterator = iterator->Next) {
			printf("%s ", iterator->Data);
		}
		printf("\n");
	}
	SLList_DeleteList(list);
}

void testList(void)
{
	char *strdup(const char *s);
	SLList_t * list;
	list = SLList_New();
	SLList_Add(list, SLLNode_Str("Hello"));
	SLList_Add(list, SLLNode_Str("World"));
	SLList_Add(list, SLLNode_Str("Og"));
	SLList_Add(list, SLLNode_Str("De"));
	PrintList(list);
	SLList_MergeSort(list, (Comparator) strcmp);
	PrintList(list);
	SLList_DeleteNodeP(list, "De", (Comparator) strcmp);
	PrintList(list);
	SLList_DeleteNodeP(list, "Og", (Comparator) strcmp);
	PrintList(list);
	SLList_DeleteNodeP(list, "World", (Comparator) strcmp);
	PrintList(list);
	SLList_DeleteNodeP(list, "Hello", (Comparator) strcmp);
	PrintList(list);
	SLList_DeleteList(list);
}

static int done = 0;
int entry = 0;

void packageReceived(Block_t * data)
{
	FILE *log = fopen(logFilename, "a");
	uint32_t * val = (void*) &data->Data[1];
#if 0
	printf("Valid package received, size = %u, signal = %u\n", data->Size, data->Data[0]);
	for(i = 0; i < 4; i++) {
		printf("measurement[%i] = %u; T[%i] = %f\n", i, val[i], i, 3.0*110.0*(double)val[i]/(double)val[3]/1023.0);
	}
#endif
	if (show)
		fprintf(stdout, "%i %f %f %f\n", entry, 3.0 * (double) val[0] / (double) val[3] / 1023.0, 3.0 * (double) val[1] / (double) val[3] / 1023.0,
				3.0 * (double) val[2] / (double) val[3] / 1023.0);
	if (log != NULL) {
		fprintf(log, "%i %f %f %f\n", entry, 3.0 * (double) val[0] / (double) val[3] / 1023.0, 3.0 * (double) val[1] / (double) val[3] / 1023.0,
				3.0 * (double) val[2] / (double) val[3] / 1023.0);
		fflush(log);
		fclose(log);
		if (plot) {
			fprintf(gnuplot,
					"plot \"%s\" using 1:2 with lines title \"A0\", \"%s\" using 1:3 with lines title \"A1\", \"%s\" using 1:4 with lines title \"A8\"\n",
					logFilename, logFilename, logFilename);
			fflush(gnuplot);
		}
	}
	entry++;
}

void print(char *str)
{
	size_t n = strlen(str);
	write(1, str, n);
}

/** \brief Function called when user has entered some data */
void userCallBack(void *extra, int fd, Block_t *data)
{
	unsigned int i;
	(void) extra;
	(void) fd;
	TERM_Color(COL_GREEN);/* Set green color*/
	for (i = 0; i < data->Size; i++) {
		switch (data->Data[i]) {
		case 'p':
			plot = !plot;
			if (plot)
				print("Plotting on.\n");
			else
				print("Plotting off.\n");
			break;
		case 's':
			show = !show;
			if (show)
				print("Show measurement on.\n");
			else
				print("Show measurement off.\n");
			break;
		case 'q':
			done = 1;
			print("Quiting program...\n");
			break;
		case 'h':
			print("p - Toggle plotting of graph\n");
			print("s - Toggle showing measurements here\n");
			print("q - Quit program\n");
			print("h - Show help\n");
			break;
		default:
			print("Press h for help.\n");
			break;
		}
	}
	TERM_Color(COL_GREY);
}

PackageHandler_t * ph;

/** \brief Function called when serial port has some data */
void serialPortCallBack(SerialPort_t *sp, int fd, Block_t *data)
{
	(void) sp;
	(void) fd;
	PH_ReceivePackage(ph, data);
}

/** \brief Function called when timer times out */
void timerCallBack(Timer_t *timer, int fd, Block_t *data)
{
	(void) timer;
	(void) fd;
	(void) data;
}

/** \brief Set up the multiplexer to handle the user, a serial port and a timer */
void testMultiplexer(char * serialDevice)
{

	/* Create a new multiplexer*/
	Multiplexer_t * mp = MP_New(8192);

	/* Setup user input as a Datasource*/
	DataSource_t *userInput = DataSource_Init(NULL, 0, userCallBack);

	/* Create a serialport object*/
	SerialPort_t *serialPortInput = SerialPort_New(serialDevice, B9600, (CallBack_t) serialPortCallBack);

	/* Create a timer object, set interval every second */
	Timer_t *timer = TIMER_New((CallBack_t) timerCallBack);
	TIMER_SetInterval(timer, 1, 0);

	ph = PH_New(packageReceived);

	/* Connect the user input to the multiplexer*/
	MP_AddDatasource(mp, userInput);

	/* Connect the serial port to the multiplexer*/
	MP_AddDatasource(mp, serialPortInput->Source);

	/* Connect timer to the multiplexer */
	MP_AddDatasource(mp, timer->Source);

	/* Run the multiplexer*/
	while (!done) {
		MP_Select(mp);
	}
	/* Close everything */
	SerialPort_Close(serialPortInput);
	SerialPort_Delete(serialPortInput);
	DataSource_Remove(userInput);
	TIMER_Remove(timer);
	PH_Remove(ph);
	/* Remove the multiplexer but not its DataSources;*/
	MP_Remove(mp);
}

void testArrayList(void)
{
	int i = 0;
	char str[] = "Hello World";
	ArrayList_t *al = AL_New(1);
	while (str[i] != '\0') {
		AL_Add(al, &str[i]);
		i++;
	}
	AL_Add(al, &str[i]);
	printf("%s\n", (char*) al->Data);
	AL_Delete(al);
}

void testpopen(void)
{
	extern FILE *popen(const char *__command, const char *__modes) __wur;
	FILE * gnuplot;
	gnuplot = popen("gnuplot", "w");
	fprintf(gnuplot, "plot sin(x), cos(2*x)\n");
	fflush(gnuplot);
	getchar();
	fclose(gnuplot);
}

int main(int argc, char *argv[])
{
	int i;
	int n;
	SLList_t * list;
	char buf[128];
	if (argc < 2)
	{
		printf("Usage:\n%s number_of_elements\n", argv[0]);
				return 0;
	}
	list = SLList_New();
	n = atoi(argv[1]);
	for(i=0; i < n; i++) {
		sprintf(buf, "Node nr. %i", i);
		SLList_Add(list, SLLNode_Str(buf));
	}
	SLList_DeleteList(list);
	return 0;
}

int main_old3(int argc, char *argv[])
{
	unsigned char buf[128];
	int i, n;
	SerialPort_t * sp;
	if (argc < 2) {
		printf("Usage:\n%s SERIAL_PORT\n", argv[0]);
		return 0;
	}
	sp = SerialPort_New(argv[1], B115200, NULL);
	for(;;) {
		n = read(sp->Source->fd, buf, 128);
		for(i=0; i < n; i++) {
			printf("%u\n", buf[i]);
		}
	}

}

int main_old2(void)
{
	testList2();
	return 0;
}

int main_old(int argc, char *argv[])
{
	FILE *f;
	COM_Init();

	if (argc < 2) {
		printf("Usage:\n%s SERIAL_PORT\n", argv[0]);
		return 0;
	}
	if (argc > 2) {
		logFilename = argv[2];
	}
	f = fopen(logFilename, "w");
	if (f != NULL)
		fclose(f);

	atexit(TERM_EnableUserEcho);
	TERM_DisableUserEcho();
	gnuplot = popen("gnuplot", "w");
	testMultiplexer(argv[1]);
	fclose(gnuplot);
	TERM_EnableUserEcho();
	return EXIT_SUCCESS;
}
