#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

#include "timer.h"
#include "common.h"

/** \brief Create a new Timer object
 * \param callBack Callback-function to be called when the timer fires.
 * \return Timer object */
Timer_t * TIMER_New(CallBack_t callBack)
{
	int fd = 0;
	Timer_t * timer = (Timer_t*)COM_MallocGC(sizeof(Timer_t));
	COM_memoryErrorCheck(timer);
	timer->TimerSettings = (struct itimerspec *)COM_MallocGC(sizeof(struct itimerspec));
	COM_memoryErrorCheck(timer->TimerSettings);
	bzero(timer->TimerSettings, sizeof(struct itimerspec));

	fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	if(fd < 0) {
		printf("Error creating timer.\n");
		exit(-1);
	}
	timer->Source = DataSource_Init((void*)timer, fd, callBack);
	timer->Source->Extra = timer;

	return timer;
}

/** \brief Set up a timer to be an interval timer
 * \param *timer Timer object to manipulate
 * \param seconds Number of seconds to wait
 * \param nanoseconds Number of nanoseconds to wait (in addition to seconds)
 * */
void TIMER_SetInterval(Timer_t *timer, __time_t seconds, __syscall_slong_t nanoseconds)
{
	timer->TimerSettings->it_interval.tv_sec = seconds;
	timer->TimerSettings->it_interval.tv_nsec = nanoseconds;
	timer->TimerSettings->it_value.tv_sec = seconds;
	timer->TimerSettings->it_value.tv_nsec = nanoseconds;
	timerfd_settime(timer->Source->fd, 0, timer->TimerSettings, NULL);
}

/** \brief Set up a timer that only fires once
 * \param *timer Timer object to manipulate
 * \param second Number of seconds to wait
 * \param nanoseconds Number of nanoseconds to wait (in addition to seconds)*/
void TIMER_SetOnce(Timer_t *timer, __time_t seconds, __syscall_slong_t nanoseconds)
{
	timer->TimerSettings->it_interval.tv_sec = 0;
	timer->TimerSettings->it_interval.tv_nsec = 0;
	timer->TimerSettings->it_value.tv_sec = seconds;
	timer->TimerSettings->it_value.tv_nsec = nanoseconds;
	timerfd_settime(timer->Source->fd, 0, timer->TimerSettings, NULL);
}

/** \brief Remove a timer
 * \param *timer Timer to remove*/
void TIMER_Remove(Timer_t *timer)
{
	close(timer->Source->fd);
	COM_Free(timer->Source);
	COM_Free(timer->TimerSettings);
	COM_Free(timer);
}
