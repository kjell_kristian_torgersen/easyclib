/** \file serialport.c Functions for easier handling and setting up a serial port with some default settings.
 * The returned serial port object is contains a DataSource object which can be used with the Multiplexer. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

#include "datasource.h"
#include "common.h"
#include "serialport.h"

/** \brief Create a new SerialPort object. Open device *device, specify baud baudrate and supply callBack callback-function which is called when data is received from serial port.
 * \param *device Device to open
 * \param baud Baudrate, not as integer, but on following form: BXXXX, for example B9600, B57600 etc. Tip: Look in the file termios.h for valid serial port baud rates for your system.
 * \param callBack Function to be called when data is received on serial port
 * */
SerialPort_t * SerialPort_New(char *device, tcflag_t baud, CallBack_t callBack)
{
	extern void bzero (void *__s, size_t __n) __THROW __nonnull ((1));
	struct termios newtio;
	SerialPort_t *sp = (SerialPort_t*) COM_MallocGC(sizeof(SerialPort_t));
	COM_memoryErrorCheck(sp);

	sp->Source = DataSource_Init((void*)sp, 0, callBack);

	sp->Source->fd = open(device, O_RDWR | O_NOCTTY);
	if (sp->Source->fd < 0) {
		perror(device);
		exit(-1);
	}

	tcgetattr(sp->Source->fd, &sp->oldtio); /* save current port settings */

	bzero(&newtio, sizeof(newtio));
	newtio.c_cflag = baud | CS8 | CLOCAL | CREAD;
	newtio.c_iflag = IGNPAR;
	newtio.c_oflag = 0;

	/* set input mode (non-canonical, no echo,...) */
	newtio.c_lflag = 0;

	newtio.c_cc[VTIME] = 0; /* inter-character timer unused */
	newtio.c_cc[VMIN] = 1; /* blocking read until 5 chars received */

	tcflush(sp->Source->fd, TCIFLUSH);
	tcsetattr(sp->Source->fd, TCSANOW, &newtio);

	return sp;
}

/** \brief Delete SerialPort object *sp
 * \param *sp Object to delete */
void SerialPort_Delete(SerialPort_t *sp)
{
	SerialPort_Close(sp);
	DataSource_Remove(sp->Source);
	COM_FreeGC(sp);
}

/** \brief Close a serial port, but don't delete object.
 * \param *sp Object to serial port to close */
void SerialPort_Close(SerialPort_t *sp)
{
	if(sp == NULL) return;
	if(sp->Source == NULL) return;

	tcsetattr(sp->Source->fd,TCSANOW,&sp->oldtio);
	close(sp->Source->fd);
}
