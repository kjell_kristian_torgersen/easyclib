/** \file common.c Misc. common functions that are used from the whole library. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


/*#define USE_GARBAGE_COLLECTOR*/

#ifdef USE_GARBAGE_COLLECTOR
#include <gc/gc.h>
#endif

#include "common.h"

void COM_Init(void)
{
#ifdef USE_GARBAGE_COLLECTOR
	GC_INIT();
#endif
}

/** \brief Check if *ptr is NULL, then exit program
 * \param *ptr Pointer to check.
 * */
void COM_memoryErrorCheck(void *ptr)
{
	if (ptr == (void*) 0) {
		exit(-1);
	}
}

/** \brief Call free if *ptr is not NULL
 * \param *ptr Memory to free. */
void COM_Free(void *ptr)
{
#ifndef USE_GARBAGE_COLLECTOR
	if(ptr != NULL) COM_FreeGC(ptr);
#else
	(void)ptr;
#endif
}

void *COM_MallocGC(size_t n)
{
#ifdef USE_GARBAGE_COLLECTOR
	return GC_MALLOC(n);
#else
	return malloc(n);
#endif
}

/** \brief Call free if *ptr is not NULL
 * \param *ptr Memory to free. */
void COM_FreeGC(void *ptr)
{
#ifndef USE_GARBAGE_COLLECTOR
	free(ptr);
#else
	(void)ptr;
#endif
}

void* COM_memdup(const void* data, size_t n)
{
   void* copy = malloc(n);
   if(copy == NULL) return NULL;
   memcpy(copy, data, n);
   return copy;
}
