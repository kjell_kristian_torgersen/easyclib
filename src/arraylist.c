/** \file arraylist.c Functions for creating and manipulating an variable sized array that can grow until there isn't any memory left. */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arraylist.h"
#include "common.h"

/** \brief Creat a new ArrayList. Specify the element size with the variable ElemSize
 * \param ElemSize Size of one element in bytes
 * \return ArrayList object*/
ArrayList_t * AL_New(size_t ElemSize)
{
	ArrayList_t *al = NULL;
	al = (ArrayList_t*)COM_MallocGC(sizeof(ArrayList_t));
	COM_memoryErrorCheck(al);
	al->Capacity = 4;
	al->Count = 0;
	al->ElemSize = ElemSize;
	al->Data = COM_MallocGC((size_t)((size_t)al->Capacity*ElemSize));
	return al;
}

void AL_Shrink(ArrayList_t *al)
{
	AL_ChangeCapacity(al, al->Count+1);
}

/** \brief Change the capacity of ArrayList *al
 * \param *al ArrayList to increase capacity
 * */
void AL_ChangeCapacity(ArrayList_t *al, size_t newCapacity)
{
	/* Keep track of old data */
	void * oldData = al->Data;

	/* Double capacity */
	al->Capacity = newCapacity;
	al->Data = COM_MallocGC((size_t)((size_t)al->Capacity*al->ElemSize));

	/* Copy back data */
	memcpy(al->Data, oldData, (size_t)((size_t)al->Count*al->ElemSize));
	COM_FreeGC(oldData);
}

/** \brief Add *element to ArrayList *al
 * \param *al Already existing ArrayList to add element to
 * \param *element Element to add to ArrayList
 * */
void AL_Add(ArrayList_t *al, void *element)
{
	/* Check if we need to increase capacity ? */
	if(al->Count + 1>= al->Capacity) {
		AL_ChangeCapacity(al, al->Capacity << 1);
	}

	/* Copy new element into array and count up */
	memcpy(al->Data + ((size_t)al->Count*al->ElemSize), element, al->ElemSize);
	al->Count++;
}

/** \brief Delete ArrayList *al and all its data
 * \param *al ArrayList to delete
 * */
void AL_Delete(ArrayList_t *al)
{
	if(al != NULL) {
		COM_Free(al->Data);
	}
	COM_FreeGC(al);
}
